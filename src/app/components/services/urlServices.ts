export const CORREO = {
    desa: 'http://localhost:8080',
    prod: 'https://appifigarantias.com',
    api: 'https://figarantias.com', 
    correo_interno: '/servidor_correo/indexinterno.php',
    correo: '/servidor_correo/index.php',
    apicert: '/apireport-saldos-consolidados/webresources/generic/',
    apinormal: '/apireport-saldos-consolidados/webresources/genericnormal/',
    apidoble: '/apireport-saldos-consolidados/webresources/genericdoble/'
     
}
