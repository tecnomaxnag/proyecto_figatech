/**
 * FACTURA DE GARANTIAS PAGINA SOFTWARE FIGARANTIAS EN IONIC 5 ANGULAR 9 MATERIAL 9 Bootstrap 4.5.3 - Agency v1 (HASTECNOLOGIA SAS)
* Copyright 2020-2021 Start HASTECNOLOGIA S.A.S 
* @author HASTECNOLOGIA S.A.S Copyright 2020-2021 The FIGARANTIAS Authors
* pathweb=(HASTECNOLOGIA SAS/menu/periodocobertura)
* pathAplicationConfig=periodocobertura.page.html
* SecurityContext: @angular/fire/auth, APPLICATION_XML,'Access-Control-Allow-Origin' (solo por peticion Get se accede a este metodo)
* periodocobertura v0.0.1 (HASTECNOLOGIA SAS/menu/creacionusuarios) 
* Copyright 2020-2021 FIGARANTIAS, Inc. 
* Licensed under MIT (HASTECNOLOGIA SAS)
 * @author HASTECNOLOGIA S.A.S
 */
import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from 'src/app/components/model/user.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { AlertPage } from 'src/app/components/alert/alert.page';
import { AuthService } from 'src/app/components/services/auth.service';
import { CargadorService } from '../../services/cargador.services';
import { Seguimiento } from '../../model/seguimiento.model';
import { CarteraService } from '../../services/cartera.service';
import { AplicarPagos } from '../../model/aplicarpago.model';
import { Intermediario } from '../../model/intermediario.model';
import { Rol } from '../../model/rol.model';
/**
 * CONTROLADOR DE LA PAGINA CREACION DE INTERMEDIARIOS SOFTWARE FIGARANTIAS
 * @author HASTECNOLOGIA S.A.S
 */
const EntityRole: Rol[] = [
  { rol: 'Todo' }, { rol: 'Ultimo Registro' }
]
@Component({
  selector: 'app-reportecobranza',
  templateUrl: 'reportecobranza.page.html',
  styleUrls: ['reportecobranza.page.scss']
})
export class ReporteCobranzaPage implements OnInit {
  user: User = JSON.parse(sessionStorage.getItem('userSession'));
  seguimientos: Seguimiento[] = [];
  pagos: AplicarPagos[] = [];
  fechareportepago: Date;
  fechareporteseguimiento: Date;
  role = EntityRole;
  rolef;
  dataSourceSeguimiento: MatTableDataSource<any> = new MatTableDataSource();
  dataSourcePagos: MatTableDataSource<any> = new MatTableDataSource();
  displayedColumnsSeguimientoExcel: string[] = ['intermediario', 'documento', 'nombredeudor', 'celular', 'direccion', 'mail', 'seguimiento', 'fechacompromiso', 'fechaproxima', 'caracterizacion', 'finllamada', 'valorapagar', 'diasmora', 'valorcapital', 'valorpago', 'creadopor', 'creadoen', 'modificadopor', 'modificadoen', 'modificacioncausa'];
  displayedColumnsPagosExcel: string[] = ['intermediario', 'documento', 'nombredeudor', 'valorpago', 'valorcapital', 'saldocapital', 'nropagare', 'rc', 'rcid', 'gac', 'intereses', 'valorgac', 'valorintereses', 'diasmora', 'valorapagar', 'creadopor', 'creadoen', 'modificadopor', 'modificadoen', 'modificacioncausa', 'observacion'];
  @ViewChild('paginatorSeguimiento', { read: MatPaginator }) paginatorSeguimiento: MatPaginator;
  @ViewChild('paginatorPagos', { read: MatPaginator }) paginatorPagos: MatPaginator;
  constructor(private cargador: CargadorService,
    private alertPage: AlertPage,
    private carteraService: CarteraService,
    private auth: AuthService) {
    this.cargador.getCargador(1500);
    this.user = JSON.parse(sessionStorage.getItem('userSession'));
  }

  ngOnInit() {
    this.auth.loginUser(this.user).then(res => {
    }, error => {
      if (error.status == 304) {
      } else if (error.status == 400) {
        this.alertPage.presentAlert("Clave incorrecta.");
      } else if (error.status == 401) {

      } else {
        this.alertPage.presentAlert("Clave o correo incorrectos. /" + error.message + ".");
      }
    });
  }

  consultarSeguimientos() {
    this.seguimientos = new Array<Seguimiento>();
    if (this.fechareporteseguimiento) {
      if (this.rolef) {
        this.alertPage.presentAlert("Espere por favor.")
        if (this.user.role === 'Super Maestro' || this.user.role === 'Coordinador') {
          var paso = false;
          if (this.rolef === "Todo") {
            this.seguimientos = new Array<Seguimiento>();
            this.carteraService.getAfsFirestore().collection("seguimiento").orderBy('index').orderBy('documento').get()
              .then((querySnapshot) => {
                this.alertPage.closeAlert();
                this.cargador.getCargador(querySnapshot.docs.length / 2);
                querySnapshot.forEach((doc) => {
                  var garantia: Seguimiento = JSON.parse(JSON.stringify(doc.data()))
                  this.carteraService.getAfsFirestore().collection("intermediarios").where("nit", "==", garantia.intermediario).get()
                    .then((querySnapshot) => {
                      querySnapshot.forEach((doc) => {
                        var intermediario: Intermediario = JSON.parse(JSON.stringify(doc.data()))
                        garantia.intermediariodes = intermediario.sigla;
                        var valida = true;
                        (garantia.marcatiempo == undefined || garantia.marcatiempo == null) ? valida = false : null;
                        if (valida) {
                          var datevalide = new Date(garantia.marcatiempo.seconds * 1000)
                          if (datevalide.getFullYear().toString().substring(0, 4) == this.fechareporteseguimiento.toString().substring(0, 4)) {
                            paso = true;
                            this.seguimientos.push(garantia)
                          }
                        } else {
                          paso = true;
                          this.seguimientos.push(garantia)
                        }
                      });
                    });
                });
                let mypromise = function functionOne() {
                  //Empieza la promesa
                  return new Promise((resolve, reject) => {
                    return setTimeout(
                      () => {
                        if (paso == true) {
                          resolve(paso);
                        } else {
                          resolve(paso);
                        }
                      }, 2000
                    );
                  });
                };
                mypromise().then(() => {
                  if (!paso) {
                    this.alertPage.presentAlert("Error! sin consolidado para este año.")
                  } else {
                    this.alertPage.presentAlert("Exito! Reporte generado.")
                    this.dataSourceSeguimiento = new MatTableDataSource<any>(this.seguimientos);
                    setTimeout(() => {
                      var elem = document.getElementById("seguimientosexcel");
                      elem.click();
                      this.dataSourceSeguimiento.paginator = this.paginatorPagos;
                    }
                    );
                  }
                })
              })
              .catch((error) => {
                console.log("Error getting documents: ", error);
              });

          }
          if (this.rolef === "Ultimo Registro") {
            var unique = [];
            this.seguimientos = new Array<Seguimiento>();
            this.carteraService.getAfsFirestore().collection("seguimiento").orderBy('index', 'desc').get()
              .then((querySnapshot) => {
                this.alertPage.closeAlert();
                this.cargador.getCargador(querySnapshot.docs.length / 2);
                querySnapshot.forEach((doc) => {
                  var garantia: Seguimiento = JSON.parse(JSON.stringify(doc.data()))
                  this.carteraService.getAfsFirestore().collection("intermediarios").where("nit", "==", garantia.intermediario).get()
                    .then((querySnapshot) => {
                      querySnapshot.forEach((doc) => {
                        var intermediario: Intermediario = JSON.parse(JSON.stringify(doc.data()))
                        garantia.intermediariodes = intermediario.sigla;
                        if (!unique.includes(garantia.documento)) { 
                          var valida = true;
                          (garantia.marcatiempo == undefined || garantia.marcatiempo == null) ? valida = false : null;
                          if (valida) {
                            var datevalide = new Date(garantia.marcatiempo.seconds * 1000)
                            if (datevalide.getFullYear().toString().substring(0, 4) == this.fechareporteseguimiento.toString().substring(0, 4)) {
                              paso = true;
                              this.seguimientos.push(garantia)
                            }
                          } else {
                            paso = true;
                            this.seguimientos.push(garantia)
                          }
                          unique.push(garantia.documento);
                        }
                        
                      });
                    });
                });
                let mypromise = function functionOne() {
                  //Empieza la promesa
                  return new Promise((resolve, reject) => {
                    return setTimeout(
                      () => {
                        if (paso == true) {
                          resolve(paso);
                        } else {
                          resolve(paso);
                        }
                      }, 2000
                    );
                  });
                };
                mypromise().then(() => {
                  if (!paso) {
                    this.alertPage.presentAlert("Error! sin consolidado para este año.")
                  } else {
                    this.alertPage.presentAlert("Exito! Reporte generado.")
                    this.dataSourceSeguimiento = new MatTableDataSource<any>(this.seguimientos);
                    setTimeout(() => {
                      var elem = document.getElementById("seguimientosexcel");
                      elem.click();
                      this.dataSourceSeguimiento.paginator = this.paginatorPagos;
                    }
                    );
                  }
                })
              })
              .catch((error) => {
                console.log("Error getting documents: ", error);
              }); 
          }
        }
        if (this.user.role === 'Intermediario' || this.user.role === 'Maestro') {
          var paso = false;
          if (this.rolef === "Todo") {
            this.seguimientos = new Array<Seguimiento>();
            this.carteraService.getAfsFirestore().collection("seguimiento").where("intermediario", "==", this.user.maestro).orderBy('index').orderBy('documento').get()
              .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                  var garantia: Seguimiento = JSON.parse(JSON.stringify(doc.data()))
                  this.carteraService.getAfsFirestore().collection("intermediarios").where("nit", "==", garantia.intermediario).get()
                    .then((querySnapshot) => {
                      querySnapshot.forEach((doc) => {
                        var intermediario: Intermediario = JSON.parse(JSON.stringify(doc.data()))
                        garantia.intermediariodes = intermediario.sigla;
                        var valida = true;
                        (garantia.marcatiempo == undefined || garantia.marcatiempo == null) ? valida = false : null;
                        if (valida) {
                          var datevalide = new Date(garantia.marcatiempo.seconds * 1000)
                          if (datevalide.getFullYear().toString().substring(0, 4) == this.fechareporteseguimiento.toString().substring(0, 4)) {
                            paso = true;
                            this.seguimientos.push(garantia)
                          }
                        } else {
                          paso = true;
                          this.seguimientos.push(garantia)
                        }
                      });
                    });
                });
                let mypromise = function functionOne() {
                  //Empieza la promesa
                  return new Promise((resolve, reject) => {
                    return setTimeout(
                      () => {
                        if (paso == true) {
                          resolve(paso);
                        } else {
                          resolve(paso);
                        }
                      }, 2000
                    );
                  });
                };
                mypromise().then(() => {
                  if (!paso) {
                    this.alertPage.presentAlert("Error! sin consolidado para este año.")
                  } else {
                    this.alertPage.presentAlert("Exito! Reporte generado.")
                    this.dataSourceSeguimiento = new MatTableDataSource<any>(this.seguimientos);
                    setTimeout(() => {
                      var elem = document.getElementById("seguimientosexcel");
                      elem.click();
                      this.dataSourceSeguimiento.paginator = this.paginatorPagos;
                    }
                    );
                  }
                })
              })
              .catch((error) => {
                console.log("Error getting documents: ", error);
              });

          }
          if (this.rolef === "Ultimo Registro") {
            this.seguimientos = new Array<Seguimiento>();
            this.carteraService.getAfsFirestore().collection("seguimiento").where("intermediario", "==", this.user.maestro).orderBy('index').orderBy('documento').get()
              .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                  var garantia: Seguimiento = JSON.parse(JSON.stringify(doc.data()))
                  paso = true;
                  this.seguimientos.push(garantia)
                });
                let mypromise = function functionOne() {
                  //Empieza la promesa
                  return new Promise((resolve, reject) => {
                    return setTimeout(
                      () => {
                        if (paso == true) {
                          resolve(paso);
                        } else {
                          resolve(paso);
                        }
                      }, 2000
                    );
                  });
                };
                mypromise().then(() => {
                  if (!paso) {
                    this.alertPage.presentAlert("Error! sin consolidado para este año.")
                  } else {
                    var seguimientosFin = new Array<Seguimiento>();
                    var llave = false;
                    var unique = [];
                    this.seguimientos.forEach(element => {
                      if (!unique.includes(element.documento)) {
                        unique.push(element.documento);
                      }
                    });
                    unique.forEach(element => {
                      this.carteraService.getAfsFirestore().collection("seguimiento").where("intermediario", "==", this.user.maestro).where("documento", "==", element).orderBy('index', 'desc').limitToLast(1).get().then((querySnapshot) => {
                        querySnapshot.forEach(element => {
                          var garantia: Seguimiento = JSON.parse(JSON.stringify(element.data()))
                          this.carteraService.getAfsFirestore().collection("intermediarios").where("nit", "==", garantia.intermediario).get()
                            .then((querySnapshot) => {
                              querySnapshot.forEach(element => {
                                var intermediario: Intermediario = JSON.parse(JSON.stringify(element.data()))
                                garantia.intermediariodes = intermediario.sigla;
                                var valida = true;
                                (garantia.marcatiempo == undefined || garantia.marcatiempo == null) ? valida = false : null;
                                if (valida) {
                                  var datevalide = new Date(garantia.marcatiempo.seconds * 1000)
                                  if (datevalide.getFullYear().toString().substring(0, 4) == this.fechareporteseguimiento.toString().substring(0, 4)) {
                                    seguimientosFin.push(garantia);
                                    llave = true;
                                  }
                                } else {
                                  seguimientosFin.push(garantia);
                                  llave = true;
                                }
                              });
                            })
                        });
                      })
                    });
                    this.cargador.getCargador(2000)
                    let mypromise = function functionOne() {
                      //Empieza la promesa
                      return new Promise((resolve, reject) => {
                        return setTimeout(
                          () => {
                            if (llave == true) {
                              resolve(llave);
                            } else {
                              resolve(llave);
                            }
                          }, 2000
                        );
                      });
                    };
                    mypromise().then(() => {
                      if (!llave) {
                        this.alertPage.presentAlert("Error! sin consolidado para este año.")
                      } else {
                        this.alertPage.presentAlert("Exito! Reporte generado.")
                        this.dataSourceSeguimiento = new MatTableDataSource<any>(seguimientosFin);
                        setTimeout(() => {
                          var elem = document.getElementById("seguimientosexcel");
                          elem.click();
                          this.dataSourceSeguimiento.paginator = this.paginatorPagos;
                        }
                        );
                      }
                    })
                  }
                })
              })
              .catch((error) => {
                console.log("Error getting documents: ", error);
              });

          }
        }
      } else {
        this.alertPage.presentAlert("Error! seleccionar filtro reporte de gestión consolidado.")
      }
    } else {
      this.alertPage.presentAlert("Error! seleccionar fecha reporte de gestión consolidado.")
    }

  }


  consultarPagos() {
    this.pagos = new Array<AplicarPagos>();
    if (this.fechareportepago) {
      this.cargador.getCargador(2000);
      if (this.user.role === 'Super Maestro' || this.user.role === 'Coordinador') {
        var paso = false;
        this.carteraService.getAfsFirestore().collection("aplicarpago").orderBy('rcid').get()
          .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
              var garantia: AplicarPagos = JSON.parse(JSON.stringify(doc.data()))
              this.carteraService.getAfsFirestore().collection("intermediarios").where("nit", "==", garantia.intermediario).get()
                .then((querySnapshot) => {
                  querySnapshot.forEach((doc) => {
                    var intermediario: Intermediario = JSON.parse(JSON.stringify(doc.data()))
                    garantia.intermediariodes = intermediario.sigla;
                    var datevalide = new Date(garantia.marcatiempo.seconds * 1000)
                    if (datevalide.getFullYear().toString().substring(0, 4) == this.fechareportepago.toString().substring(0, 4)) {
                      paso = true;
                      this.pagos.push(garantia)
                    }
                  });
                });
            });
            let mypromise = function functionOne() {
              //Empieza la promesa
              return new Promise((resolve, reject) => {
                return setTimeout(
                  () => {
                    if (paso == true) {
                      resolve(paso);
                    } else {
                      resolve(paso);
                    }
                  }, 2000
                );
              });
            };
            mypromise().then(() => {
              if (!paso) {
                this.alertPage.presentAlert("Error! sin consolidado para este año.")
              } else {
                this.alertPage.presentAlert("Exito! Reporte generado.")
                this.dataSourcePagos = new MatTableDataSource<any>(this.pagos);
                setTimeout(() => {
                  var elem = document.getElementById("pagosexcel");
                  elem.click();
                  this.dataSourcePagos.paginator = this.paginatorPagos;
                }
                );
              }
            })
          })
          .catch((error) => {
            console.log("Error getting documents: ", error);
          });
      }
      if (this.user.role === 'Intermediario' || this.user.role === 'Maestro') {
        var paso = false;
        this.carteraService.getAfsFirestore().collection("aplicarpago").where("intermediario", "==", this.user.maestro).orderBy('rcid').get()
          .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
              var garantia: AplicarPagos = JSON.parse(JSON.stringify(doc.data()))
              this.carteraService.getAfsFirestore().collection("intermediarios").where("nit", "==", garantia.intermediario).get()
                .then((querySnapshot) => {
                  querySnapshot.forEach((doc) => {
                    var intermediario: Intermediario = JSON.parse(JSON.stringify(doc.data()))
                    garantia.intermediariodes = intermediario.sigla;
                    var datevalide = new Date(garantia.marcatiempo.seconds * 1000)
                    if (datevalide.getFullYear().toString().substring(0, 4) == this.fechareportepago.toString().substring(0, 4)) {
                      paso = true;
                      this.pagos.push(garantia)
                    }
                  });
                });
            });
            let mypromise = function functionOne() {
              //Empieza la promesa
              return new Promise((resolve, reject) => {
                return setTimeout(
                  () => {
                    if (paso == true) {
                      resolve(paso);
                    } else {
                      resolve(paso);
                    }
                  }, 2000
                );
              });
            };
            mypromise().then(() => {
              if (!paso) {
                this.alertPage.presentAlert("Error! sin consolidado para este año.")
              } else {
                this.alertPage.presentAlert("Exito! Reporte generado.")
                this.dataSourcePagos = new MatTableDataSource<any>(this.pagos);
                setTimeout(() => {
                  var elem = document.getElementById("pagosexcel");
                  elem.click();
                  this.dataSourcePagos.paginator = this.paginatorPagos;
                }
                );
              }
            })
          })
          .catch((error) => {
            console.log("Error getting documents: ", error);
          });
      }
    } else {
      this.alertPage.presentAlert("Error! seleccionar fecha reporte de pagos consolidado.")
    }

  }


}




