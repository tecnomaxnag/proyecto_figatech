import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './components/services/auth.service';
import { Router } from '@angular/router';
import { User } from './components/model/user.model';

@Injectable()
export class Guard implements CanActivate {

  constructor(private user: AuthService, private router: Router) { }



  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    var returnUser= true;
    var user = this.user.getUserLoggedIn(); 
    var userSession : User = JSON.parse(sessionStorage.getItem('userSession')); 
    if (!user) {
      (user  == null || user == false) ? returnUser = false : null;  
    }
    if(!returnUser){
      var validateuserSession = true;
      (userSession  == null || userSession.email == "") ? validateuserSession = false : null; 
      if(validateuserSession){
        this.user.loginUser(userSession)
        sessionStorage.setItem('userSession',JSON.stringify(userSession));
        returnUser=true;
      }else{
        this.router.navigate(['login']);
      }
    } 
    return returnUser;
  }
}